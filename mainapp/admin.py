from django.contrib import admin
from mainapp.models import RequestedQuote, Service, ProjectPhoto, Project, PersonalInfo

# Register your models here.
admin.site.register(RequestedQuote)
admin.site.register(Service)
admin.site.register(ProjectPhoto)
admin.site.register(Project)
admin.site.register(PersonalInfo)

