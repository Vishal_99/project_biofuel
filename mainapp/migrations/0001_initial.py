# Generated by Django 3.2.6 on 2021-09-19 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PersonalInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=15, null=True)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('hours', models.CharField(max_length=20, null=True)),
                ('facebook', models.URLField(null=True)),
                ('twitter', models.URLField(null=True)),
                ('youtube', models.URLField(null=True)),
                ('youtube_video', models.URLField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectPhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('photo', models.ImageField(upload_to='static/ProjectPhoto/')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='RequestedQuote',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location', models.TextField(max_length=1000)),
                ('survey_no', models.CharField(max_length=200)),
                ('located_at', models.CharField(choices=[('WITHIN_CITY_LIMIT', 'Within City Limit'), ('OUTSIDE_CITY_LIMIT', 'Outside City Limit')], max_length=30)),
                ('landmark', models.TextField(max_length=200)),
                ('investment', models.PositiveIntegerField()),
                ('min_area_requirement', models.BooleanField()),
                ('description', models.TextField(max_length=10000)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('phone_number', models.CharField(max_length=15)),
                ('email', models.EmailField(max_length=256)),
                ('preferred_contact_type', models.CharField(choices=[('all', 'All'), ('email', 'Email'), ('phone', 'Phone')], max_length=100)),
                ('address', models.TextField(max_length=1000)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=100)),
                ('zip_code', models.CharField(max_length=10)),
                ('country', models.CharField(max_length=90)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.TextField(max_length=100)),
                ('title_summary', models.TextField(max_length=1000)),
                ('feature1', models.TextField(max_length=250)),
                ('feature2', models.TextField(max_length=250)),
                ('feature3', models.TextField(max_length=250)),
                ('overview', models.TextField()),
                ('pic1', models.ImageField(blank=True, upload_to='static/services/')),
                ('pic2', models.ImageField(blank=True, upload_to='static/services/')),
                ('stats_pic', models.ImageField(blank=True, upload_to='static/services/')),
                ('stats_text', models.TextField(max_length=250)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title_pic', models.ImageField(blank=True, upload_to='static/Project/')),
                ('title', models.TextField(max_length=200)),
                ('content', models.TextField(max_length=5000)),
                ('client', models.CharField(max_length=200)),
                ('location', models.CharField(max_length=500)),
                ('services', models.CharField(max_length=500)),
                ('conclusion_title', models.TextField(max_length=250)),
                ('conclusion_text', models.TextField(max_length=3000)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('images', models.ManyToManyField(to='mainapp.ProjectPhoto')),
            ],
        ),
    ]
