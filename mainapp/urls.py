from django.urls import path
from .views import *

urlpatterns=[
    path("", IndexView.as_view()),
    path("aboutus/",AboutusView.as_view()),
    path("how-it-works/", HowItWorksView.as_view()),
    path("team/", LeadershipView.as_view()),
    path("awards/", AwardsView.as_view()),
    path("pricing/", PricingView.as_view()),
    path("faqs/", FAQsViews.as_view()),
    path("gallery/", GalleryView.as_view()),
    path("career/", CareerView.as_view()),
    path("shop/", ShopView.as_view()),
    path("quote/", QuoteView.as_view()),
    path("services/",ServiceListView.as_view()),
    path("services/<pk>/", ServicesDetailView.as_view()),
    path("projects/",ProjectListView.as_view()),
    path("projects/<pk>/",ProjectListView.as_view()),
]
