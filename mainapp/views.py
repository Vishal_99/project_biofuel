from django.shortcuts import render, redirect
from django.views.generic import View, TemplateView, DetailView
from mainapp.forms import RequestedQuoteForm
from mainapp.models import RequestedQuote
from .models import *


# Create your views here.
class IndexView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['service'] = Service.objects.all()
        context['service_list'] = Service.objects.filter(include_in_dropdown_list=True)
        context['project-list'] = Project.objects.filter(include_in_dropdown_list=True)
        return context

class AboutusView(TemplateView):
    template_name = 'page-about.html'

class HowItWorksView(TemplateView):
    template_name = 'page-how-works.html'

class LeadershipView(TemplateView):
    template_name = 'page-team.html'

class AwardsView(TemplateView):
    template_name='page-awards.html'

class PricingView(TemplateView):
    template_name='page-pricing.html'

class FAQsViews(TemplateView):
    template_name='page-faqs.html'

class GalleryView(TemplateView):
    template_name='page-gallery.html'

class CareerView(TemplateView):
    template_name='page-careers.html'

class ShopView(TemplateView):
    template_name='shop-products.html'

class QuoteView(TemplateView):
    template_name = 'request-quote.html'
    quote_form = RequestedQuoteForm()
    success_url = "/"

    def post(self, request):
        quote_form_submitted = RequestedQuoteForm(request.POST)
        if quote_form_submitted.is_valid():
            quote_form_submitted.save(commit=True)
            return redirect('/')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['quote_form'] = self.quote_form
        return context

class ServiceListView(TemplateView):
    template_name = "page-services.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['service'] = Service.objects.all()
        return context

class ServicesDetailView(DetailView):
    model = Service
    template_name = "service-turbines.html"

class ProjectListView(TemplateView):
    template_name = "projects-grid.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['project'] = Project.objects.all()
        return context

class ProjectDetailView(DetailView):
    model = Project
    template_name = "projects-single.html"