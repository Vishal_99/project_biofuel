from django.forms import ModelForm
from .models import RequestedQuote

class RequestedQuoteForm(ModelForm):
    class Meta:
        model = RequestedQuote
        fields = '__all__'
