from django.db import models

# Create your models here.
class RequestedQuote(models.Model):
    LOCATED_AT_CHOICES = [
        ('WITHIN_CITY_LIMIT','Within City Limit'),
        ('OUTSIDE_CITY_LIMIT','Outside City Limit'),
    ]

    CONTACT_PREFERENCE_CHOICES = [
        ('all','All'),
        ('email','Email'),
        ('phone','Phone'),
    ]

    location = models.TextField(max_length=1000)
    survey_no = models.CharField(max_length=200)
    located_at = models.CharField(max_length=30, choices=LOCATED_AT_CHOICES)
    landmark = models.TextField(max_length=200)
    investment = models.PositiveIntegerField()
    min_area_requirement = models.BooleanField()
    description = models.TextField(max_length=10000)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)
    email = models.EmailField(max_length=256)
    preferred_contact_type = models.CharField(max_length=100, choices=CONTACT_PREFERENCE_CHOICES)
    address = models.TextField(max_length=1000)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=10)
    country = models.CharField(max_length=90)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.first_name + " " + self.last_name
    



class Service(models.Model):
    title = models.TextField(max_length=100)
    title_summary = models.TextField(max_length=1000)
    feature1 = models.TextField(max_length=250)
    feature2 = models.TextField(max_length=250)
    feature3 = models.TextField(max_length=250)
    overview = models.TextField()
    pic1 = models.ImageField(upload_to='static/services/', blank=True)
    pic2 = models.ImageField(upload_to='static/services/', blank=True)
    stats_pic = models.ImageField(upload_to='static/services/', blank=True)
    stats_text = models.TextField(max_length=250)
    include_in_dropdown_list = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.title


class ProjectPhoto(models.Model):
    title = models.CharField(max_length=50)
    photo = models.ImageField(upload_to='static/ProjectPhoto/')
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.title



class Project(models.Model):
    title_pic = models.ImageField(upload_to='static/Project/', blank=True)
    title = models.TextField(max_length=200)
    content = models.TextField(max_length=5000)
    client = models.CharField(max_length=200)
    location = models.CharField(max_length=500)
    services = models.CharField(max_length=500)
    conclusion_title = models.TextField(max_length=250)
    conclusion_text = models.TextField(max_length=3000)
    images = models.ManyToManyField(ProjectPhoto)
    include_in_dropdown_list = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)
    def __str__(self):
        return self.title



class PersonalInfo(models.Model):
    phone = models.CharField(max_length=15, null=True)
    email = models.EmailField(null=True)
    hours = models.CharField(max_length=20, null=True)
    facebook = models.URLField(null=True)
    twitter = models.URLField(null=True)
    youtube = models.URLField(null=True)
    youtube_video = models.URLField(null=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.email



